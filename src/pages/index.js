import React from "react"
import { Link, StaticQuery, graphql } from "gatsby"
import Layout from "../components/layout"
import SEO from "../components/seo"
import "../gstyle.css"
import _ from 'lodash'
import Profile from "../components/Profile";


const IndexPage = () => (
  <Layout>
    <SEO title="Dev " />

    <Profile />

    <StaticQuery key="allSitePage" query={graphql`
    
      query{
        allSitePage{
          edges{
              node{
                  context{
                      type
                      title
                      path
                      year
                  }
              }
          }
        }
      }
    `}
      render={data => {
        //if no data just render /
        if (!data || data.allSitePage.edges.length == 0) return "/"


        let edges = _.filter(data.allSitePage.edges,e=>e.node.context!=null)



        let groups = _.groupBy(edges, edge => edge.node.context.type)

        let fields = _.filter(_.keys(groups), k => k != "null") //post,article,experince

        return <div className="main-container">
      
      <div className="my-ul-container">
          {


            fields.map(field => <ul className="my-ul">
            <li className={`li-title-${field}`} style={{fontFamily: "Dancing Script"}}><b>{field}s</b></li>
            {
  
              (() => {
  
                switch (field) {
                  case 'experience':
                    return groups[field].map((ed, k) =>
                      <li className={`li-${ed.node.context.type}`} key={k}><Link to={ed.node.context.path} ><h5>{ed.node.context.title}</h5></Link><span className="exp-year">{ed.node.context.year}</span></li>)
                  default:
                    return groups[field].map((ed, k) =>
                      <li className={`li-${ed.node.context.type}`} key={k}><Link to={ed.node.context.path} ><h5>{ed.node.context.title}</h5></Link></li>)
                }
              })()
  
            }
          </ul>)
  
          }
        </div>

        </div>
      }}
    />

  </Layout>
)




export default IndexPage
