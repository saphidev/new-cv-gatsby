

module.exports = [
    {
        type:"post",
        path:"/looking-for-remote-work/",
        image:"looking.svg",
        title:"I'm looking for a remote work",
        content:`
***Hello*** ,

this is an open cover letter, that I hope will draw your attention. my name is Safi , Graduated with Master Degree in Computer Science.
        
An expert in React + Redux and a good experience with Graphql and Gatsby ,with over 3 years of experience as a web applications developer. My approach to software development, a continues workflow and set goals first and balancing both optimization and products requirements.
        
**PROGRAMMING**
        
Mentoring and pair-programming are really rewarding, I excel, draw, note at planning and developing solutions for long-term timescales. and as for programming languages, I know JavaScript and Golang really well,and can write programs in Java and .net (although I’m still learning those languages).    
I can also read a number of other programming languages like Python.
this is an example of a gatsby web i made for a friend [itqan-dz](https://itqan-dz.com) deployed on Netlify, also i have created a sample School Manager web application using React , Redux, Material-ui , Mongodb and Nodejs [https://moonacademy.surge.sh](https://moonacademy.surge.sh).
        
**SKILLS & TECHNOLOGIES**
        
React + React Native , GraphQl + RestAPIs , Nodejs + Javascript , Golang , JWT+AWS Serveless + CI + Docker + Gitlab, Firebase , Postgresql + GraphQl.
          
        
**ADDITIONAL ACTIVITIES**
        
curious about how things works, love cats.
        
and this is my [Github](https://github.com/apotox)
`
    },
    {
        type:"article",
        path:"/netlify-and-firebase-admin/",
        title:"Firebase Admin with Netlify lambda functions",
        source:"https://medium.com/@saphidev/use-firebase-admin-with-netlify-lambda-functions-free-483d3b390e3a",
        content:`On this project I will create two parts, the first is the front-end page and the second is our backend which is a lambda function.`
    }
,
    {
        type:"experience",
        path:"/exp/",
        title:"Full-stack Developer",
        year:"2017 – Present",
        content:`using Reactjs i started my first full time job as a web applications developer,
        my work was creating a web interface for ERP's clients.`
    },
    {
        type:"experience",
        path:"/freelance/",
        title:"Freelance Web Apps Developer (Upwork)",
        year:"2018 – 2019",
        content:"Working as a web developer and data scraping on Upwork and Fiverr. use link bellow to hire me ^^",
        source:"https://www.upwork.com/freelancers/~012f7d7bee0af19274"
    }

]